import os

from pathlib import Path
from filecmp import cmp
from shutil import copyfile


def test():
    """test."""

    PARENT = Path(__file__).parent
    script = str(PARENT / "ressources/many_functions.py")
    s_target = str(PARENT / "ressources/many_functions_TARGET.py")
    s_clean = str(PARENT / "ressources/many_functions_CLEAN.py")
    tests = str(PARENT / "ressources/many_functions_test.py")

    try:
        from ressources.optimus import Optimus
        OP = Optimus(
            script,
            tests,
            verbose=False
        )
        OP.optimize()
    except:
        pass

    test = False
    if cmp(script, s_target):
        test = True

    os.remove(script)
    copyfile(s_clean, script)

    return test
