from setuptools import setup, find_packages

setup(
    name='optimus',
    version='0.1',
    packages=find_packages(),
    author='Denis Maurel',
    author_email='d.maurel@groupeonepoint.com',
    description='Code debloating using Delta Debugging and Reinforcement Learning'
)
