"""
== MODELS==
Best so far : tree.DecisionTreeClassifier
Tested:
- [04/2020] ensemble.RandomForestClassifier -> too time consuming to train
- [04/2020] linear_model -> can result in negative probabilities

== TEST FILE DEFINITION ==
Regarding the definition of the test file used by Optimus to debloat, it is imperative that
every import used by the test() function is located *inside* the test function. For an example
you can take a look at examples/test_easy/many_functions_test.py.
The point of this is to avoid the test function to import things that would break the standard
behavior of Optimus.
"""

import ast as _ast
import io as _io
import logging as _logging
import os as _os
import sys as _sys
from copy import deepcopy as _deepcopy
from importlib import import_module as _import_module
import multiprocessing as _mp
from pathlib import Path as _Path
from time import time as _time

import astor as _astor
import autopep8
import matplotlib.pyplot as _plt
import numpy as _np
from flake8.api import legacy as flake8
from sklearn import tree as _tree
from sklearn.preprocessing import MultiLabelBinarizer as _MLB

from .utils import module_utils as _module_utils
from .utils import tree_utils as _tree_utils
from .utils import test_utils as _test_utils
from .utils import decorators as _decorators

logger = _logging.getLogger()


class Optimus():
    """
    Optimus.

    This program is a replication of the algorithm defined in the scientific paper
    https://dl.acm.org/doi/pdf/10.1145/3243734.3243838.

    This class is the package main class. Being given the code and the test (property) to pass,
    the class takes care of the whole optimizing process. Optimus uses two kind of algorithm which
    work together in order to debloat the code:

    - Delta Debbuging [http://web2.cs.columbia.edu/~junfeng/09fa-e6998/papers/delta-debug.pdf] is
      an algorithm which recursively consider subparts of a program, remove them from the original
      code, and see if the resulting code can pass a specific test (the test can be a real life
      application of the code, like creating an archive for tar)

    - Reinforcement Learning [https://en.wikipedia.org/wiki/Reinforcement_learning] is a Machine
      Learning paradigm in which an agent is put in an environment and learn how to react to this
      environment through many episodes of test-error. In our case, the agent try to learns which
      part of the code is mandatory for the program to pass the test, and which are not.

    Some concepts to better understand the algorithm:
    - The test is a single function which attest that the program can do a set of actions. It is
      different from unit test in that only one boolean is returned each time.
    - We are looking for a program that is 1-minimal, which means that the program must pass the
      test and fail it if we remove any line from its code.
    """

    def __init__(self,
                 code,
                 test=None,
                 module_folder=None,
                 verbose=False,
                 test_timeout=1,
                 test_framework=None,
                 debloated_folder=None,
                 duration_delta=1,
                 one_way=False,
                 fast_fail=False,
                 no_rl=False):
        """
        Optimus constructor

        :param code: the file containing the code to be optimized
        :param test: the file containing the test to be run
        :param module_folder: the folder containing the files to optimize
        :param verbose: specify if the class should explain what is does
        """

        self.RL = not no_rl

        # If no test is provided, raise an Exception
        if test is None and test_framework is None:
            raise Exception('Either test of test_framework parameter has to be provided.')

        if test is not None and test_framework is not None:
            raise Exception('Can\t consider both a test file and a test framework.')

        if test_framework is not None and module_folder is None:
            raise Exception('Optimus requires a folder to use a test_framework.')

        # Does Optimus needs to talk
        self.verbose = verbose

        # Specify if Optimus should do several iteration to reach perfect optimization
        self.one_way = one_way

        # The original file containing the code to debloat
        self.original_file = code

        # Is the target is a whole module, useful also to get tests if tested with a framework
        self.module_folder = module_folder

        # Folder with debloated files
        self.debloated_folder = debloated_folder

        # Append module folder to path to make it findable by self.test()
        if module_folder:
            _sys.path.insert(0, str(_Path(module_folder).parent))

        # Create the original Abstract Syntax Tree of the target code
        code_file = _io.open(code, 'r')
        self.tree = _ast.parse(code_file.read())
        code_file.close()

        # Convert the filename of the test as a module path and import the test function in it
        self.ALLOWED_FRAMEWORKS = {
            'pytest': _test_utils._import_pytest_test(module_folder)
        }
        self.test = self._import_test(test, test_framework)

        # How long in seconds a test can last before being terminated as _ast
        self.test_timeout = test_timeout

        # Max time a test can take without being considered worse than the previous optimal one
        self.duration_delta = duration_delta

        # Current best time of execution for a test. Test_timeout is used by default because its
        # the max value a test can take
        self.current_best_time = test_timeout

        # Class used to prune useless parts of the original code
        self.transformer = _tree_utils.Transformer()

        # Class used to explore the code when the process begins
        self.visitor = _tree_utils.Visitor()

        # Define each node id as being its hash
        self.visitor.hash_tree(self.tree)

        # Define file syntax checker
        self.fast_fail = fast_fail
        if self.fast_fail:
            logger.setLevel(_logging.ERROR)
            self.checker = flake8.get_style_guide(
                ignore=['E501', 'E741', 'E722'],
                select=['E']
            )
            logger.setLevel(_logging.INFO)

        # Get all the nodes contained in the code
        self.program = self.visitor.get_all_statements(self.tree)

        # Get roots of all subtrees needed for local debloating
        self.subtrees = self.visitor.get_list_subtrees(self.tree)

        # The program of the current subtree
        self.subprogram = self.program

        # Current subtree
        self.subtree = self.tree

        # Current investigated depth
        self.depth = 1

        # If the original program does not pass the test, no need to continue
        if not self.test_program(self.program)['result']:
            if self.verbose:
                msg = f'Test invalid : the original {self.original_file} does not pass'
                logger.error(msg)
                raise Exception(msg)

    def _import_test(self, test, test_framework):
        """
        Method used to define the test function used for this optimization.
        If test_framework has been defined, it will overwrite anything specified by
        the test parameter.

        :param test: The python test file provided to Optimus
        :param test_framework: The name of the test framework to used
        :returns: a test function
        """

        # It a test framework is specified
        if test_framework is not None:

            # If the test framework is allowed
            if test_framework in self.ALLOWED_FRAMEWORKS:
                return self.ALLOWED_FRAMEWORKS[test_framework]

            # If not, it might be a typo, so abort the run
            else:
                keys = self.ALLOWED_FRAMEWORKS.keys()
                logger.error(f'test_framework parameter should be in {keys}')
                _sys.exit()

        elif test is not None:
            # If only test is specified, import the file as a module and get the test function
            path = _Path(test)
            _sys.path.insert(0, str(path.parent))
            return _import_module(path.stem).test
        else:
            raise Exception('test and test_framework are both None')

    def get_program_by_depth(self, depth):
        """
        Get all the node eligible for the optimization at the specified depth

        :param depth: the depth of the node to return
        :returns: the list of the program item to optimize
        """

        # Consider only the code of the current debloated program
        tree = self.transformer.clean(
            ast_tree=_deepcopy(self.tree),
            target_program=self.partial_program,
            subprogram=self.subprogram,
            depth=depth - 1
        )

        # Get lit of all nodes
        flatten_tree = self.visitor.flatten_tree(tree, subprogram=self.subprogram)
        # Only consider nodes with corresponding depth
        return [node.hash for node in flatten_tree if node.depth == depth]

    def put_sample_in_dataset(self, sample, results):
        """
        Convert the sample and put it along with its test result in the dataset

        :param sample: program item list
        :param pass_test: test result
        """

        # Convert program into a binary vector
        data = self.encoder.transform([sample])

        # Formating results to be sure that boolean are always in same order
        results = results['result']

        self.dataset.append([data[0], _np.array(results)])

    def train_model(self):
        """
        Train the model based on the current dataset
        """

        self.model = self.model.fit(
            [x[0] for x in self.dataset],
            [x[1] for x in self.dataset]
        )

    def generate_children(self, program):
        """
        Generate all the children of the program based on the current granularity

        :param program: the program to get children of
        :returns: the list of all the possible children
        """

        children = []
        # list to keep track of already generated children
        already = []
        n_nodes = len(program)
        # Length of a code partition. A partition is a subset of the program
        len_partition = n_nodes / self.granularity

        # Partitions
        for i in range(self.granularity):
            # Begin and end indices of the current partition
            begin = round(i * len_partition)
            end = round((i + 1) * len_partition)
            elem = program[begin:end]

            # The granularity of the child is the granularity to use it passes the test
            child = {
                'elem': elem,
                'granularity': 2,
                'type': 'partition'
            }

            elem = tuple(elem)  # The tuple trick is here to allow the "in" python test
            # If the partition is valid and not already considered, consider it
            if elem not in already\
                    and elem not in self.done\
                    and len(elem) > 0:
                already.append(elem)
                children.append(child)

        # For each partition, we also consider its complement, i.e. all the node that
        # are in the program but not in the partition
        for i in range(self.granularity):
            # Begin and end indices of the current partition
            begin = round(i * len_partition)
            end = round((i + 1) * len_partition)
            elem = program[:begin] + program[end:]

            # The granularity of the child is the granularity to use if the child is
            # chosen and pass the test
            child = {
                'elem': elem,
                'granularity': self.granularity - 1,
                'type': 'complement'
            }

            elem = tuple(elem)  # The tuple trick is here to allow the "in" python test
            # If the partition is valid and not already considered, consider it
            if elem not in already\
                    and elem not in self.done\
                    and len(elem) > 0:
                already.append(elem)
                children.append(child)

        # If no child is good enough, we consider the original program
        # with a finer granularity (worst case scenario)
        children.append({
            'elem': self.current_program,
            'granularity': 2 * self.granularity,
            'type': 'all_fail'
        })

        return children

    def evaluate_child(self, child):
        """
        Get the probability of the child to be 1-minimal based on the current model

        :param child: the child to test
        :returns: the probability for the child to pass the test
        """

        # All children to be tested for 1 - minimality
        children = [child[:i] + child[i + 1:] for i in range(len(child))]
        # Transform all children in binary vectors
        children = self.encoder.transform(children)

        # Get the probability that the program is 1-minimal, which implies that
        # all its children fail the test
        pass_test = self._model_predict(children)
        value = (_np.ones(len(children)) - pass_test).prod()
        return value

    def _model_predict(self, target):
        """
        Utilitary function to get model predictions

        :param target: The target which prediction has to be computed
        :returns: A list of the tweaked probabilities
        """

        # To make the next line consistent
        if not hasattr(target, '__iter__'):
            target = _np.array([target])

        # Original predictions
        pass_test = _np.float64(self.model.predict(target))

        return pass_test

    def get_proba_pass_test(self, child, siblings):
        """
        Get the probability for a child to pass the test.
        Siblings of the child (in terms of children generated by generate_children) are
        required in order to compute the probability of the worst case scenario child.

        :param child: the program to be tested
        :param siblings: the other probram generated by generate_children
        :returns: probability to pass the test according to the current model
        """

        # Binarize the child
        data = self.encoder.transform([child['elem']])
        # If the child is not a worst case scenario one, simply get the model output
        if child['type'] != 'all_fail':
            value = self._model_predict(data)
            return value

        # If the worst case scenario is not alone
        if len(siblings):
            # We consider the probability for all the other siblings to fail
            siblings = self.encoder.transform(siblings)
            pass_test = self._model_predict(siblings)
            value = (1 - pass_test).prod()
        else:
            # Else, return a probability of 1 if it is the only candidate
            value = 1
        return value

    def rank_children(self, children):
        """
        Rank a set of children based on their probabilities to be 1-minimal and on their
        probabilities to pass the test. Both values are required to compute the value
        of the solution regarding Reinforcement Learning rules.

        :param children: all the children to choose from
        :returns: the most promising candidate
        """

        # Get the probability of 1-minimality for all children
        values = [self.evaluate_child(child['elem']) for child in children]

        # Get the probability of passing the test for all children
        probas = _np.array([
            self.get_proba_pass_test(
                child,
                [x['elem'] for x in children if x is not child]
            ) for child in children
        ])

        # Normalize proba to sum to 1
        if probas.sum():
            probas = probas / probas.sum()

        # Get the Q value from the Reinforcement Learning paradigm
        values *= probas

        # Rank children by decreasing values and put the "all_fail" case at the end
        all_fail = children[-1]
        children = [children[i] for i in _np.array(values[:-1]).argsort()[::-1]]

        # Make sure that the "all_fail" case is at the end
        children.append(all_fail)

        return children

    @_decorators.silent
    def test_syntax_file(self, file):
        """Test if a file is grammaticaly correct

        :param file: the file to test
        :returns: a boolean
        """

        # Avoid flake8 to report everything it does
        logger.setLevel(_logging.ERROR)
        report = self.checker.check_files([file])
        logger.setLevel(_logging.INFO)

        # Check that there is not errors
        res = report.get_statistics('E') == []

        return res

    def test_program(self, program):
        # Back up of modules before the test
        previous_state = _module_utils.save_modules_state()

        # Write current version of the code
        tree_cleaned = self.transformer.clean(
            ast_tree=_deepcopy(self.tree),
            target_program=program,
            subprogram=self.subprogram,
            depth=self.depth
        )

        # Write it to a file
        with _io.open(self.original_file, 'w') as f:
            code = _astor.to_source(tree_cleaned)
            if self.fast_fail:
                code = autopep8.fix_code(code)
            f.write(code)
            f.close()

        # Fast fail using a syntaxic checker
        if self.fast_fail and not self.test_syntax_file(self.original_file):
            results = {'result': 0, 'duration': 0}
        else:
            # If we target a folder rather than a file
            if self.module_folder:

                # Extract everything that could interfere with the debloated module
                list_modules = _module_utils.extract_module(self.module_folder)

                # Delete currently loaded module to avoid interferences with the test
                _module_utils.delete_modules(list_modules)

            # Get results and check execution time
            results = self._get_results_test_program()
            self.update_results_using_execution_time(results)

            # Restore module states as it was before the test
            _module_utils.restore_previous_module_state(previous_state)

            # If positive test, update max_depth
            if results['result']:

                # Get max_depth of current tested tree
                all_nodes = self.visitor.flatten_tree(tree_cleaned, subprogram=self.subprogram)

                # Max depth of new subprogram
                max_depth = max([node.depth for node in all_nodes])
                self.max_depth = max_depth

        return results

    def update_results_using_execution_time(self, results):
        """
        Utilitary function to update results based on test execution time

        :param results: Results returned by test_program
        """

        time = self.current_best_time

        # duration_delta is here to mitigate any system imprecision
        if results['duration'] <= time + self.duration_delta:
            # Keep the result, whatever it is
            self.current_best_time = results['duration']
        else:
            # If time exceeds requirements, set result to 0
            results['result'] = 0

    def _get_results_test_program(self):
        """
        Test the input program with the test given to Optimus

        :param program: the program to be tested
        :returns: a boolean representing the result of the program to the test
        """

        # Test it with the test
        queue = _mp.SimpleQueue()
        p = _mp.Process(target=self._test_with_return, args=(queue,))
        p.start()
        p.join(self.test_timeout)
        if p.is_alive():
            p.terminate()
            p.join()
            test = {'result': 0, 'duration': self.test_timeout}
        else:
            test = queue.get()

        return test

    @_decorators.silent
    def _test_with_return(self, queue):
        """
        Utilitary function to make the test run in a process to have a timeout available

        :param queue: the multiprocessing.Queue object to share the test result
        """
        start = _time()
        try:
            test = 1 * self.test()
        except:
            test = 0
        duration = _time() - start

        # Put the result in queue
        result = {'result': test, 'duration': duration}
        queue.put(result)

    @_decorators.silent
    def _subtest_with_return(self, subtest, queue):
        """
        Utilitary function to make the test run in a process to have a timeout available

        :param queue: the multiprocessing.Queue object to share the test result
        """
        start = _time()
        try:
            test = 1 * self.test(subtest)
        except:
            test = 0
        duration = _time() - start

        # Put the result in queue
        result = {'subtest': subtest, 'result': test, 'duration': duration}
        queue.put(result)

    def _make_ready_for_subprogram(self, subprogram):

        """
        Initialize everything before starting an optimization on a subprogram.

        :param subprogram: the program describing the current subprogram.
        """

        # All the nodes that have been optimized so far
        self.partial_program = [self.subtree.hash]

        # Current subprogram of subtree
        self.subprogram = subprogram

        # Select the max of the last info of the program items
        self.max_depth = self.visitor.get_max_depth(self.tree, self.subprogram)
        self.depth = self.subtree.depth + 1

        # Get the node to be analyzed and optimized
        self.current_program = self.get_program_by_depth(self.subtree.depth + 1)

        # To make each program understandable by the model
        # Used to convert a list of program item into a binary vector
        self.encoder = _MLB().fit([self.subprogram])

        # The model used to predict if a program will pass the test or not
        self.model = _tree.DecisionTreeClassifier()

        # The list containing all the tested programs so far with their test results
        self.dataset = []

        # Hyperparameter of the model, in how many pieces should the program be cut
        self.granularity = 2

        # all the program tested so far
        self.done = [tuple(self.subprogram)]

        # The original program pass the test, it is added to the dataset to initiate
        # the model training
        results = {'result': 1}

        self.put_sample_in_dataset(self.subprogram, results)
        self.train_model()

    def optimize(self, plot=False):
        """ Optimize using subtrees """
        stats = []

        # If the original program is useless
        if self.test_program([self.tree.hash])['result'] or len(self.program) == 0:
            self.write_program_to_file([self.tree.hash])
            return [{'original_file': self.original_file, 'ratio': -1}]

        optimizable = True
        while optimizable:

            # We consider that this iteration is the last one by default
            optimizable = False

            # First, optimize everything except function content and get some stats about it
            self.subtree = self.tree
            if self.verbose:
                logger.info('Debloating skeleton')
            subprogram = self.visitor.get_program_without_subtrees(self.tree)
            stat = self._optimize_subprogram(subprogram, plot)
            stats.append(stat)

            # If a file has changed, another iteration might be useful
            if not self.one_way and stat['n_line_start'] != stat['n_line_end']:
                optimizable = True

            # Second, optimize each function content ie each subtree found in the original tree
            for i, subtree in enumerate(self.subtrees):
                if self.verbose:
                    logger.info(f'Function {i+1}/{len(self.subtrees)}: {subtree.name}')
                self.subtree = subtree

                # Get the program description to optimize
                subprogram = self.visitor.get_all_statements(
                    tree=self.tree,
                    subtree=subtree
                )

                # If the subprogram has not been removed by the global optimization
                if subprogram:
                    stat = self._optimize_subprogram(subprogram, plot)
                    stats.append(stat)

                    # If a file has changed, another iteration might be useful
                    if not self.one_way and stat['n_line_start'] != stat['n_line_end']:
                        optimizable = True

        stats = self._aggregate_stats(stats)
        return stats

    def _aggregate_stats(self, stats):
        """
        Utilitary function to aggregate optimization stats.

        :param stats: the stats to aggregate
        :returns: the aggregated stats
        """
        # Get all subtree names, barbaric method instead of set to keep function order
        names = []
        for stat in stats:
            name = stat['name']
            if name not in names:
                names.append(name)

        # Aggregate all stats by name
        new_stats = []
        for name in names:
            sublist = [stat for stat in stats if stat['name'] == name]
            start = max([item['n_line_start'] for item in sublist])
            end = min([item['n_line_end'] for item in sublist])
            new_stat = {
                'name': name,
                'original_file': sublist[0]['original_file'],
                'ratio': round(100 * (start - end) / start),
                'n_line_start': start,
                'n_line_end': end,
                'duration': sum([item['duration'] for item in sublist]),
                'iterations': sum([item['iterations'] for item in sublist])
            }
            new_stats.append(new_stat)

        return new_stats

    def _optimize_subprogram(self, subprogram, plot=False):
        """
        The main method. Launch the optimization process

        :param plot: if the class should plot the resulting model
        :returns: an object with the debloated program and some more infos
        """

        # Initiliaze everything that needs to be initialized
        self._make_ready_for_subprogram(subprogram)

        # Iteration counter
        counter = 0
        if self.verbose:
            logger.info('iter   depth   pass   gran   len(elem)')
            logger.info('-' * 38)

        start = _time()
        # While there is something to investigate
        while self.depth <= self.max_depth:
            while True:
                # Get all possible next program
                children = self.generate_children(self.current_program)

                # If every possibility has been tested
                if len(children) == 1 and self.granularity > len(self.current_program):
                    break

                # Select the next program following greedy policy
                children_ranked = children
                if self.RL:
                    children_ranked = self.rank_children(children)

                for child in children_ranked:
                    elem = child['elem']
                    gran = child['granularity']

                    # Test current program
                    results = self.test_program(self.partial_program + elem)
                    test = results['result']

                    # Log and update model
                    counter += 1
                    if self.RL:
                        self.put_sample_in_dataset(self.partial_program + elem, results)
                    self.done.append(tuple(elem))
                    if self.verbose:
                        logger.info('{:4}   {:2}/{:2}   {:4}   {:4}   {:9}'.format(
                            counter, self.depth, self.max_depth, test, self.granularity, len(elem)
                        ))

                    if self.RL:
                        self.train_model()

                    if test:
                        break

                # Update current state
                self.current_program = elem
                self.granularity = gran
                self._update_tree(
                    new_program=self.partial_program + elem,
                    subprogram=self.subprogram
                )

            # Go to next depth and reinitialize the parameters
            self.depth += 1
            self.granularity = 2
            self.partial_program += self.current_program
            self.current_program = self.get_program_by_depth(self.depth)

            # If there is nothing more to debloat
            if not self.current_program:
                break

        # When optimization is finished, return usable stats
        duration = _time() - start
        stats = self._return_stats_optim(duration, plot)
        stats['iterations'] = counter

        return stats

    def _update_tree(self, new_program, subprogram):
        """
        Utilitarty function to update working tree based on the currently investigated subtree and
        the considered subprogram.

        :param new_program: The currently investigated subprogram.
        :param subprogram: The subtree investigated.
        """

        # Tree cleaning and updating
        tree_cleaned = self.transformer.clean(
            ast_tree=_deepcopy(self.tree),
            target_program=new_program,
            subprogram=self.subprogram,
            depth=self.depth
        )
        self.tree = tree_cleaned
        self.subtrees = self.visitor.get_list_subtrees(self.tree)

    def _return_stats_optim(self, duration, plot):
        """
        Utilitary function to compute and return useful stats.

        :param duration: The duration taken by the debloating process.
        :param plot: Boolean precising if the model has to be plotted or not.
        """

        if self.verbose:
            logger.info(f'Duration : {duration:6.1f}s')

        # Write debloated program to file
        self.write_program_to_file(self.partial_program)

        # Stats about reduction ratio
        length1 = len(self.partial_program)
        length2 = len(self.subprogram)
        ratio = round((length2 - length1) / length2 * 100)

        if self.verbose:
            logger.info(f'Optimization ratio : {ratio}%\n')
        if plot:
            self.plt_model()

        # Getting either function name for local debloating or module for global one.
        name = 'module' if not hasattr(self.subtree, 'name') else f'-- {self.subtree.name}'

        return {
            'name': name,
            'original_file': self.original_file,
            'ratio': ratio,
            'n_line_start': length2,
            'n_line_end': length1,
            'duration': duration
        }

    def write_program_to_file(self, program):
        """
        Write input program to file

        :param program:
        """

        # Clean the original tree following input program
        tree_cleaned = self.transformer.clean(
            ast_tree=_deepcopy(self.tree),
            target_program=program,
            subprogram=self.subprogram,
            depth=self.depth
        )

        # Write optimized version of the code
        _os.remove(self.original_file)
        with _io.open(self.original_file, 'w') as f:
            code = _astor.to_source(tree_cleaned)
            if self.fast_fail:
                code = autopep8.fix_code(code)
            f.write(code)
            f.close()

    def plt_model(self):
        """
        Plot underlying model of Optimus.
        """

        _plt.figure(figsize=(17, 10))
        _tree.plot_tree(
            self.model,
            filled=True,
            rounded=True,
            class_names=('fail', 'pass'),
            fontsize=10,
            feature_names=self.program
        )
        _plt.show()
