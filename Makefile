e = examples
o = optimus
ts = sklearn
toa = optimus_astor
tao = auto_optimus
ex = extras
st = stevedore

install:
	pip install -r requirements.txt
	ln -s $(o) lib/python3.8/site-packages/$(o)

examples:
	@echo "Updating files to make $(e) runnable"
	@rm -rf $(e)/$(ts)/sklearn
	@rm -rf $(e)/$(toa)/astor
	@rm -rf $(e)/$(toa)/ressources/$(o)
	@rm -rf $(e)/$(tao)/$(o)
	@rm -rf $(e)/pytest_$(ex)/$(ex)
	@rm -rf $(e)/pytest_$(st)/$(st)
	@cp -r ./lib/*/site-packages/sklearn/ $(e)/$(ts)/sklearn
	@cp -r ./lib/*/site-packages/astor $(e)/$(toa)/astor
	@cp -r ./lib/*/site-packages/astor $(e)/$(toa)/astor
	@cp -r ./lib/*/site-packages/$(ex) $(e)/pytest_$(ex)/$(ex)
	@cp -r ./lib/*/site-packages/$(st) $(e)/pytest_$(st)/$(st)
	@cp -r $(o) $(e)/$(toa)/ressources/$(o)
	@cp -r $(o) $(e)/$(tao)/$(o)

clean_all:
	@rm -rf $(e)/**/debloated*
	@rm -rf $(e)/*/*.txt

run_example:
	@python -m $(o) -c $(e)/$(TARGET)/config.yaml

.PHONY: run_example clean_all examples install