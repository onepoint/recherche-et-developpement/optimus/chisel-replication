#!/usr/bin/env python

"""
Main script for Optimus Prime

Allow the use of optimus as a standalone program.

If your test requires some external files, you have to put everything in an additionnal
ressources folder at the same level as the test file.

If the script to debloat is a folder, optimus will scan recursively the folder
and optimize and python file found which is not an __init__.py .
"""

import argparse
import io
import logging
import sys
import datetime
import yaml
import time
from os import listdir
from os.path import isdir, isfile
from pathlib import Path
from subprocess import call

from optimus import Optimus

logging.basicConfig(
    format='%(levelname)s : %(message)s',
    level=logging.INFO)

ap = argparse.ArgumentParser()
ap.add_argument('-c', '--configure', required=False,
                help="The YAML file used to described the debloating process")
ap.add_argument('-p', '--program', required=False,
                help="The file of the program to debloat")
ap.add_argument('-t', '--test', required=False,
                help="The file of the test for the program to pass")
ap.add_argument('-v', '--verbose', default=False, required=False, action="store_true",
                help="Enter verbose mode")
ap.add_argument('-m', '--model', default=False, required=False, action="store_true",
                help="Print final learned tree model")
ap.add_argument('--one-way', default=False, required=False, action="store_true",
                help="Perform only one pass of optimization")
ap.add_argument('-f', '--test-framework', required=False,
                help="Test framework to use instead of --test, will overwrite it")
ap.add_argument('--test-timeout', default=1, required=False, type=int,
                help="Maximum time a test can last before being aborted")
ap.add_argument('--duration-delta', default=1, required=False,
                help="Maximum time delta allowed for a test execution time")
ap.add_argument('--from-module', required=False,
                help="Module containing the file to debloat")
ap.add_argument('--fast-fail', required=False,
                help="Perform a syntax check before running the test")
ap.add_argument('--no-rl', required=False, default=False, action='store_true')

args = vars(ap.parse_args())


def get_list_files_to_debloat(path):
    """
    Recursively explore the target path and get all the python files.

    @param path: path of the file/folder to explore
    @return: a list of python file paths
    """

    files = []

    # If the target is a python file
    if isfile(path) and path[-3:] == '.py':
        files.append(path)

    # If the target is a folder
    elif isdir(path):
        # Create a running list of all the folders to explore
        list_dir = [path]

        # Recursive exploration of the folder to find all python files
        for folder in list_dir:
            for target in listdir(folder):
                file = folder + '/' + target

                # If it is a python file, add it to the files to debloat
                if isfile(file) and target[-3:] == '.py':
                    files.append(file)

                # If it is a folder, add it to the folders to explore
                if isdir(file):
                    list_dir.append(file)

    return files


def create_debloated_folder(path_code, path_test, path_module=None):
    """
    Create the debloated folder at the target location and copy required files in it.

    @param path: target location
    @return: object containing path to debloated folder and its main components
    """

    # Define root of debloated folder
    target = path_module if path_module else path_code
    folder = str(Path(target).parent / 'debloated_')

    # Dating
    folder += datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

    # Folder creation
    call(['mkdir', folder])

    if path_module:
        # If a module is specified, copy it first
        # No need to copy path_code since it is supposed to be in path_module
        call(['cp', '-r', path_module, folder])
        module = folder + '/' + Path(path_module).name

        # Case for which a whole module is debloated
        source = module

        # If it is only a specific subset of the module
        if path_code != path_module:
            source += '/' + str(Path(path_code).relative_to(path_module))
    else:
        # If no module is specified, copy code
        call(['cp', '-r', path_code, folder])
        module = None
        source = folder + '/' + Path(path_code).name

    # Copy path_test to the root of debloated folder
    test = None
    if path_test is not None:
        call(['cp', path_test, folder])
        test = folder + '/' + Path(path_test).name

        # If additionnal files are required for the tests to run
        ressources = str(Path(path_test).parent / 'ressources')
        if isdir(ressources):
            call(['cp', '-r', ressources, folder])

    debloated = {
        'folder': folder,
        'source': source,
        'test': test,
        'module': module
    }

    return debloated


if __name__ == '__main__':
    # Loading yaml conf file if any
    if args['configure']:
        with io.open(args['configure'], 'r') as file:
            config = yaml.load(file, Loader=yaml.FullLoader)
            args = {**args, **config}
            file.close()

    TARGET = args['program']
    TESTS = args['test']

    # Minimum tests to check if TARGET and TESTS are usable
    try:
        if TESTS is not None:
            assert (isfile(TESTS) and TESTS[-3:] == '.py')
        assert isdir(TARGET) or (isfile(TARGET) and TARGET[-3:] == '.py')
    except AssertionError:
        logging.error('Wrong input format.')
        logging.error('--program must be either a folder or a python file')
        sys.exit()

    # If any test framework is defined, check if from_module is present
    if args['test_framework']:
        try:
            assert args['from_module'] is not None
        except Exception as excp:
            logging.error("--test-framework requires --from_module to be set.")
            raise excp

    # Duplicate files to avoid altering original ones
    debloated = create_debloated_folder(TARGET, TESTS, args['from_module'])

    # Allow import of debloated modules rather than regular ones
    sys.path.insert(0, debloated['folder'])

    # Files to debloat
    files = get_list_files_to_debloat(debloated['source'])
    if len(files) == 0:
        logging.warning('No files to debloat')

    logging.info('Tests : {}\n'.format(TESTS))
    logging.info(f'Files to debloat : {len(files)}')
    useless_files = 0

    # Reduction ratios to display
    ratios = []
    start = time.time()
    for i, file in enumerate(files):
        logging.info(f'[{i+1}/{len(files)}] {file.split("/")[-1]}')

        # If the file is empty
        with io.open(file, 'r') as open_file:
            if open_file.read() == '':
                res = [{
                    'original_file': file,
                    'ratio': -1,
                }]
            else:
                # Where the magic happens
                OP = Optimus(
                    file,
                    debloated['test'],
                    verbose=args['verbose'],
                    module_folder=debloated['module'],
                    test_framework=args['test_framework'],
                    test_timeout=args['test_timeout'],
                    duration_delta=args['duration_delta'],
                    one_way=args['one_way'],
                    fast_fail=args['fast_fail'],
                    no_rl=args['no_rl']
                )
                res = OP.optimize(plot=args['model'])
                optimization_duration = time.time() - start

        # A ratio of -1 is a convention to display a useless file
        if res[0]['ratio'] == -1:
            useless_files += 1
        else:
            ratios.append(res)

    # Delete test file from debloated folder
    if debloated['test']:
        call(['rm', debloated['test']])

    # Printing summary after debloating
    logging.info(f'Useless files: {useless_files}\n')
    logging.info(f'Optimization duration: {optimization_duration:6.1}\n')

    # Get the max filename length for display purpose
    names = [item['name'].split('/')[-1] for file_item in ratios for item in file_item]
    lengths = [len(item) for item in names]
    max_len = max(lengths)

    # Logging summary
    logging.info('file' + ' ' * (max_len - 2) + 'ratio   before   after   duration   iterations')
    logging.info('=' * (max_len + 48))
    total_duration = 0
    total_iterations = 0
    for file_item in ratios:
        original_file = file_item[0]["original_file"].split('/')[-1]
        logging.info(original_file)
        for item in file_item:
            name = item['name']
            ratio = item['ratio']
            before = item['n_line_start']
            after = item['n_line_end']
            duration = item['duration']
            total_duration += duration
            iteration = item['iterations']
            total_iterations += iteration
            line = f'{name}'
            line += ' ' + ' ' * (max_len - len(name)) + '  '
            line += f'{ratio:3.0f}%     {before:4}    {after:4}     {duration:6.1f}'
            line += f'   {iteration:10}'
            logging.info(line)

    with io.open(f'{args["program"]}.txt', 'a') as file:
        logging.info(f'writing info in {args["program"]}.txt')
        file.write(f"\nTOTAL DURATION : {total_duration}")
        file.write(f"\nTOTAL ITERATIONS : {total_iterations}")