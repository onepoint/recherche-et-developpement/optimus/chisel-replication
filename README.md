# Chisel Replication

## Installation

### Install package
It is recommended to create a python virtualenv in order to ensure that everything is properly installed.
Using [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/index.html), you can follow these steps:

```shell
$ git clone https://gitlab.com/onepoint/recherche-et-developpement/optimus/chisel-replication.git
$ mkvirtualenv chisel-replication
$ cd chisel-replication
```

Then, install the requirements and install optimus locally:

```shell
$ pip install -r requirements.txt
$ pip install -e optimus
```

### Install toy examples
To be able to run toy examples, first ensure that you have `make` installed. If you're on Windows, you can refer to this [Stack Overflow post](https://stackoverflow.com/questions/2532234/how-to-run-a-makefile-in-windows) to know how to run a `Makefile` on your machine.

Then, you need to make the project folder a virtualenv. This way, all dependencies are installed locally and the following command will be able to copy every required folder in corresponding `examples` folder:
```sh
$ make examples
```

If the `lib` folder containing all dependencies is not in the `chisel-replication` folder, you will get the following error:
> `cp: [...] './lib/site-packages/sklearn/': No such file or directory`

## Use

### Module as standalone program

optimus is runable as a standalone program. To use it, just run:
```sh
$ python -m optimus -c path/to/config.yaml
```

The `config.yaml` file is used to define every configuration parameter. An example can be found 
[here](https://gitlab.com/onepoint/recherche-et-developpement/optimus/chisel-replication/-/blob/master/examples/easy/config.yaml).

### Toy examples

You can run a specific example using:
```sh
$ make run_example TARGET='<test_name>'
```
with `<test_name>` in:
- auto_optimus
- easy
- optimus_astor
- sklearn
- pytest_extras
- pytest_stevedore

If you want to remove all debloated folders at once, run:
```sh
$ make clean_all
```

Please note that for each modified file, a `<filename>_CLEAN.py` file exists. The project uses these files as backup, and will duplicate them each time the original file is modified by the system. This way, it ensures that each example will work as intended.

Also, please note that toy examples files should not be modified. For each file, a `<filename>_TARGET.py` has been created, this latter file being the expected result of the optimisation performed by the system on the original file. To modify the original file may imply that the `TARGET` file will no longer be correct.

Enjoy ^^.
