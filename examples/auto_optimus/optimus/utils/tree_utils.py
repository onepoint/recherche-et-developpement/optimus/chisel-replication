import ast as _ast


def print_tree(tree, depth=0):
    """Utilitary function to print a whole tree

    :param tree: The tree to print
    """
    print('  ' * depth + tree.__class__.__name__)
    for child in get_node_children(tree):
        print_tree(child, depth=depth + 1)


def get_node_children(node):
    """
    Utilitary function to get all available children of a specific node.

    :param node: parent node to get children from
    """
    children = []
    # For every key value pair in the parent node
    for field, value in _ast.iter_fields(node):
        if isinstance(value, list):
            for item in value:
                # The child has to be a node and must not be a docstring
                if isinstance(item, _ast.AST):
                    if not hasattr(item, 'is_docstring'):
                        children.append(item)

    return children


class Transformer(_ast.NodeTransformer):
    """
    Class used to transform an existing AST into a different one based on different
    program item lists
    """

    def __init__(self):
        """
        Transformer constructor
        """

        self.program = []       # The program to based the pruning on
        self.current_depth = 0  # Define the depth until which the Transformer as to investigate

    def clean(self, ast_tree, target_program, subprogram, depth):
        """
        Clean input tree based on the program item list : every node encountered which is not
        in program will be removed along with all its children.

        :param tree:
        :param program:
        :returns: cleaned tree
        """

        # Set the program to make it accessible everywhere
        self.target_program = target_program
        self.subprogram = subprogram
        # Set the max depth to prune
        self.current_depth = depth
        # Prune the tree
        new_tree = self.visit(ast_tree)
        _ast.fix_missing_locations(new_tree)
        return new_tree

    def generic_visit(self, node):
        """
        Define the generic behavior of a Transformer exploring an AST

        :param node:
        :returns: a possibly modified node
        """

        # If it's a docstring, keep it
        if hasattr(node, 'is_docstring'):
            return node

        # If the node is not in program and if its depth is still accessible
        if node.hash not in self.target_program\
                and node.hash in self.subprogram\
                and node.depth <= self.current_depth:
            return None

        # Iteratively go through the tree and prune nodes.
        for field, value in _ast.iter_fields(node):
            if isinstance(value, list):
                replacement = []
                for item in value:
                    if isinstance(item, _ast.AST):
                        new_item = self.visit(item)
                        if new_item is not None:
                            replacement.append(new_item)
                # For a reference problem, the original value object has to be kept
                value[:] = replacement

        return node


class Visitor(_ast.NodeVisitor):
    """
    Visitor is used to gather information on an AST tree
    """

    def get_max_depth(self, tree, subprogram):
        """
        Utilitary function which get the maximum depth in the tree (without docstrings)

        :param tree: The tree investigated
        :param depth: a recursive parameter to get the current max depth
        :returns: max depth found in the tree
        """

        all_nodes = self.flatten_tree(tree, subprogram=subprogram)
        max_depth = max([node.depth for node in all_nodes])
        return max_depth

    def hash_tree(self, tree, depth=0):
        """
        Recursively defile id and depth of each node in the tree

        :param tree: the tree to hash
        """

        tree.depth = depth
        tree.hash = hash(tree)
        self.test_presence_docstring(tree)
        for node in get_node_children(tree):
            self.hash_tree(node, depth + 1)

    def flatten_tree(self, tree, subprogram=[], all_nodes=None):
        """
        Return the complete list of nodes of an input tree

        :param tree: the tree to flatten
        :param subprogram: the subprogram from which get the nodes
        :param all_nodes: internal parameter used to recursively get the node list
        :returns: a list of ast.Node
        """

        # If the node is in current subprogram or if no subprogram is provided
        if tree.hash in subprogram or not subprogram:
            if all_nodes is None:
                all_nodes = [tree]
            else:
                all_nodes.append(tree)

        # Continue recursively to add children to the list
        children = get_node_children(tree)
        for child in children:
            all_nodes = self.flatten_tree(
                tree=child,
                subprogram=subprogram,
                all_nodes=all_nodes)

        return all_nodes

    def get_all_statements(self, tree, subtree=None):
        """
        Main method of Visitor, used to get all the node identifiers available in a tree

        :param tree: the tree to explore
        :param subtree: the subtree to only consider
        :returns: a list of all node id
        """

        # Get all node hashes
        all_nodes = self.flatten_tree(tree)
        all_hashs = [node.hash for node in all_nodes
                     if not hasattr(node, 'is_docstring')]

        # If a subtree is provided, focus on it
        if subtree:
            if subtree.hash in all_hashs:
                return self.get_all_statements(
                    tree=subtree,
                    subtree=None
                )
            else:
                return None

        return all_hashs

    def test_presence_docstring(self, node):
        """
        Hack to issue #7

        TODO : https://gitlab.groupeonepoint.com/research/optimus-prime/optimus-prime/issues/7
        Hack to avoid the bug with one line docstring.
        Test the first child of the node, and tag it as docstring if necessary
        :param node: the node to check
        """

        name_class = node.__class__.__name__
        if name_class in ['Module', 'ClassDef', 'FunctionDef']:
            if hasattr(node, 'body'):
                if isinstance(node.body, list):
                    if len(node.body) > 0:
                        candidate = node.body[0]
                        if candidate.__class__.__name__ == 'Expr':
                            if hasattr(candidate.value, 'value'):
                                if isinstance(candidate.value.value, str):
                                    candidate.is_docstring = True

    def get_list_subtrees(self, tree):
        """
        Utilitary function to get list of all subtrees in tree.

        :param tree: The original tree.
        :returns: A list of all subtrees in tree.
        """

        # Get all nodes in original tree
        all_nodes = self.flatten_tree(tree)

        # A subtree is basically a node defining a function
        subtrees = [node for node in all_nodes if node.__class__.__name__ == 'FunctionDef']

        # Sort the subtrees by depth to optimize debloating process
        sorted(subtrees, key=lambda node: node.depth)
        return subtrees

    def get_program_without_subtrees(self, tree, flush=True, program=[]):
        """
        Utilitary function to get list of all node hashs which are not in a subtree.

        :param tree: The original tree.
        :param program: Internal parameter to recursively get the list of all not-subtree nodes.
        :returns: a list of all node hashes in the original program without its subtrees.
        """

        if flush:
            program.clear()

        # If it went this far, the node is not in a subtree
        program.append(tree.hash)

        # If the node is not a subtree root
        if tree.__class__.__name__ != 'FunctionDef':
            children = get_node_children(tree)
            for child in children:
                program = self.get_program_without_subtrees(child, flush=False, program=program)

        return program
