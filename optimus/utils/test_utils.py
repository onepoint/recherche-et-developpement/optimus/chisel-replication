"""
Module used to center every test framework related function.
The point is to have one function per framework, a function being linked to Optimus.test
and being invoked by Optimus.test_program().

Regarding imports, it might be a good idea to keep everything framework related inside the
related function, in order to avoid installing every framework before using optimus.
"""

import multiprocessing as _mp
from .decorators import silent


# Utility class used as a plugin to gather node ids
class NodeIdsCollector:
    def pytest_collection_modifyitems(self, items):
        self.nodeids = [item.nodeid for item in items]


@silent
def _get_list_subtests(source, test_timeout=3):

    def _func(queue, source):
        import pytest

        # Plugin initialization and node ids gathering
        collector = NodeIdsCollector()
        pytest.main([source, '--collect-only'], plugins=[collector])

        # Sometimes, node ids also contain path to the source folder, so it needs to me removed
        # for consistency
        nodeids = collector.nodeids
        nodeids = [nodeid.replace(source, '') for nodeid in nodeids]

        queue.put(nodeids)

    queue = _mp.SimpleQueue()
    p = _mp.Process(target=_func, args=(queue, source,))
    p.start()
    p.join(test_timeout)
    if p.is_alive():
        p.terminate()
        p.join()
        tests = []
    else:
        tests = queue.get()

    return tests


def _import_pytest_test(source):
    """
    Pytest framework test function
    """

    def test():
        import pytest

        # Return 0 if all tests passed successfully, else something > 0
        # See https://docs.pytest.org/en/stable/usage.html for more info
        exit_code = pytest.main([source, "--exitfirst"])
        return exit_code == 0

    return test