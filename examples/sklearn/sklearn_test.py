from multiprocessing import Process
from os import remove
from os.path import isfile
from pathlib import Path

import numpy as np


def test():

    def _practical_test():
        """practical_test.

        :param script:
        :param tests:
        :param verbose:
        """
        try:
            from sklearn.linear_model import LinearRegression
            X = np.array([1, 2, 3, 4]).reshape(-1, 1)
            y = np.array([3, 6, 9, 12])
            reg = LinearRegression().fit(X, y)
            if 'coef_' in dir(reg):
                open(Path(__file__).parent / 'success.test', 'w').close()
        except:
            pass

    p = Process(target=_practical_test)
    p.start()
    p.join(2)
    if p.is_alive():
        p.terminate()
        p.join()

    test_value = False
    path = Path(__file__).parent / 'success.test'
    if isfile(path):
        test_value = True
        remove(path)

    return test_value
